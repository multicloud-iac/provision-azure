terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.64.0"
    }
  }
  required_version = ">= 0.13"
}

provider "azurerm" {
  features {}
}

# CREATE RESOURCE GROUP
resource "azurerm_resource_group" "vm_resource_group" {
  name     = "${var.dns_prefix}-resource-group"
  location = var.location
  tags = {
    Project = var.dns_prefix
  }
}

# Create VM
module "server" {
  depends_on = [azurerm_resource_group.vm_resource_group]

  source  = "Azure/compute/azurerm"
  version = "3.14.0"

  resource_group_name = "${var.dns_prefix}-resource-group"
  location            = var.location

  vm_hostname          = "${var.dns_prefix}-vm"
  vm_size              = var.vm_size
  vm_os_simple         = "UbuntuServer"
  storage_account_type = "Standard_LRS"

  vnet_subnet_id          = module.network.vnet_subnets[0]
  public_ip_dns           = [var.dns_prefix]
  remote_port             = 22
  source_address_prefixes = [var.ssh_cidr]
  enable_ssh_key          = false
  admin_password          = var.admin_password

  tags = {
    Name    = "${var.dns_prefix}-vm"
    Project = var.dns_prefix
  }
}

# Create Networking
module "network" {
  depends_on = [azurerm_resource_group.vm_resource_group]

  source  = "Azure/network/azurerm"
  version = "3.5.0"

  resource_group_name = "${var.dns_prefix}-resource-group"

  tags = {
    Name    = "${var.dns_prefix}-network"
    Project = var.dns_prefix
  }
}

output "vm_public_name" {
  value = module.server.public_ip_dns_name
}

