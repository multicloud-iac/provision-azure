# Variables

variable "dns_prefix" {
  description = "DNS prefix to add to to public IP address for VM"
  default     = "tfcb-test"
}

variable "vm_size" {
  description = "Azure VM Size"
  default     = "Standard_D2s_v3"
  # default   = "Standard_D3s_v3"   # type out of policy
  # default   = "Standard_D5_v2"    # high cost
}

variable "ssh_cidr" {
  description = "IP CIDR to allow inbound on port 22."
  default     = "10.0.0.1/32"
}

variable "admin_password" {
  description = "admin password for Windows VM"
  default     = "pTFE1234!"
}

variable "location" {
  description = "Azure location in which to create resources"
  default     = "West US 2"
}
